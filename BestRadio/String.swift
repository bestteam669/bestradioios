//
//  String.swift
//  BestRadio
//
//  Created by Admin on 30/09/2018.
//  Copyright © 2018 SA Cowabunga. All rights reserved.
//

import Foundation

extension String {
    
    static func className(_ aClass: AnyClass) -> String {
        return NSStringFromClass(aClass).components(separatedBy: ".").last!
    }
    
    
    /// Localized version of this string
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.index(self.startIndex, offsetBy: from))
    }
    
    var length: Int {
        return self.count
    }
    
    func indexOf(_ char: String) -> Int {
        if self.range(of: char) == nil {
            return -1
        }
        
        let range: Range<String.Index> = (self.range(of: char))!
        let index: Int = self.distance(from: self.startIndex, to: range.lowerBound)
        return index
    }
    
    func substringyIndex(string: String, startOffset: Int, endOffset: Int) -> String { //f.e  (2, 1) "Hello" -> "ll"
        let start = string.index(string.startIndex, offsetBy: 1)
        let end = string.index(string.endIndex, offsetBy: -1)
        let range = start..<end
        let mySubstring = string[range]
        return mySubstring.description
    }
    
    func validatedURL() -> String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
    }
}
