//
//  ViewController.swift
//  BestRadio
//
//  Created by Admin on 15/09/2018.
//  Copyright © 2018 SA Cowabunga. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Firebase
import FirebaseDatabase
import SwiftyJSON
import MoPub
import MediaPlayer

class ViewController: UIViewController {
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var djImage: UIImageView!
    @IBOutlet weak var contactBtn: UIButton!
    
    
    var fireBaseRef: DatabaseReference!
    var databaseHandle: DatabaseHandle!
    var player: AVPlayer!
    var lastDjImage: UIImage!
    var playPauseBtn: UIButton!
    
    let radiomngr = RadioManager.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customizeNavigationBar()
        fireBaseRef = Database.database().reference()
        fetchProducerInfo()
        drawCircleBottom()
        drawCircleTop()
        playPauseBtn.tag = 1
        
        if !radiomngr.isTimerRunning {
            radiomngr.startTimer()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(fillInData), name: NSNotification.Name.init("ProducerInfo"), object: nil)
        
        initializeMopub()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.init("ProducerInfo"), object: nil)
    }
    
    @objc func fillInData(_ notification: Notification) {
        
        let userInfo = notification.userInfo
        let json = userInfo?["JSON"] as! JSON
        let name = json["name"].stringValue
        self.infoLabel.text = name
        let imageUri = json["imageUri"].stringValue
        let dataDecoded : Data = Data(base64Encoded: imageUri, options: .ignoreUnknownCharacters)!
        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        self.djImage.image = decodedimage
        lastDjImage = decodedimage
        
        if player != nil {
            setupNowPlaying(image: lastDjImage)
        }
    }
    
    func fetchProducerInfo() {
        
        let currDay = DayTimeUtils.getDayOfWeek()
        let isWeekend = currDay == "Saturday" || currDay == "Sunday"
        let index = DayTimeUtils.getFirebaseIndex(isWeekend: isWeekend, currentHour: DayTimeUtils.getCurrentHour())
        let path = "schedule/" + currDay + "/" + String(index)
        databaseHandle = fireBaseRef.child(path).observe(.value, with: { (snapshot) in
            let json = JSON(snapshot.value as Any)
            let name = json["name"].stringValue
            self.infoLabel.text = name
            let imageUri = json["imageUri"].stringValue
            let dataDecoded : Data = Data(base64Encoded: imageUri, options: .ignoreUnknownCharacters)!
            let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
            self.djImage.image = decodedimage
            self.djImage.layer.cornerRadius = 64
            self.djImage.clipsToBounds = true
            self.lastDjImage = decodedimage
        })
    }
    
    func customizeNavigationBar() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        self.navigationController?.navigationBar.topItem?.title = "Best Radio 926"
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
    
    func playAudio() {
        guard let url = URL(string: "http://best926live.mdc.akamaized.net/strmBest/userBest/chunks.m3u8") else {
            return
        }
        // Create an AVPlayer, passing it the HTTP Live Streaming URL.
        if player == nil {
            player = AVPlayer(url: url)
            setupRemoteTransportControls()
            
            if lastDjImage == nil {
                lastDjImage = UIImage.init(named: "best926")
            }
            setupNowPlaying(image: lastDjImage)
        }
        player.play()
        
    }
    
    func stopAudio() {
        
        if player != nil {
            player.pause()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func drawCircleBottom() {
        let screenWidth = self.view.frame.width
        let screenHeight = self.view.frame.height
        let hiddenSize = screenWidth / 4
        
        
        let circleView = UIView(frame: CGRect.init(x: 0 - hiddenSize, y: (screenHeight - screenWidth) + hiddenSize, width: screenWidth, height: screenWidth))
        circleView.backgroundColor = UIColor.init(hex: "ab1313")
        circleView.layer.cornerRadius = circleView.frame.width/2
        circleView.clipsToBounds = true
        
        let btnSize = circleView.frame.height / 8
        playPauseBtn = UIButton.init(frame: CGRect.init(origin: circleView.center, size: CGSize.init(width: btnSize, height: btnSize)))
        playPauseBtn.setBackgroundImage(#imageLiteral(resourceName: "play_icon"), for: .normal)
        playPauseBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        self.view.addSubview(playPauseBtn)
        
        self.view.addSubview(circleView)
        
//        self.view.bringSubview(toFront: playPauseBtn)
//        self.view.bringSubview(toFront: contactBtn)
        
        self.view.sendSubview(toBack: circleView)
    }
    
    func drawCircleTop() {
        let screenWidth = self.view.frame.width/2
        let _ = self.view.frame.height
        let hiddenSize = screenWidth / 4
        
        
        let circleView = UIView(frame: CGRect.init(x: (self.view.frame.width - screenWidth) + hiddenSize, y: 0 - hiddenSize, width: screenWidth, height: screenWidth))
        circleView.backgroundColor = UIColor.init(hex: "ab1313")
        circleView.layer.cornerRadius = circleView.frame.width/2
        circleView.clipsToBounds = true
        
        self.view.addSubview(circleView)
        
    }
    
    func initializeMopub() {
        let sdkConfig = MPMoPubConfiguration(adUnitIdForAppInitialization: "3e15052dc5534b90acafa5a51bc118c8")
        sdkConfig.globalMediationSettings = nil
        
        MoPub.sharedInstance().initializeSdk(with: sdkConfig, completion: {
            print("MoPub has been initialized . . . . . .")
            if (MoPub.sharedInstance().shouldShowConsentDialog) {
                MoPub.sharedInstance().loadConsentDialog(completion: { (error) in
                    if (error == nil) {
                        MoPub.sharedInstance().showConsentDialog(from: self, didShow: {
                            print("didShow")
                        }, didDismiss: {
                            print("didDismiss")
                        })
                    } else {
                        print("failed to load consent dialog")
                    }
                })
            } else {
                print("no need for consent dialog");
            }
        })
    }
    
    @objc func buttonAction() {
        if playPauseBtn.tag == 1 {
            playAudio()
            playPauseBtn.setBackgroundImage(UIImage(named:"pause_icon.png"),for:UIControlState.normal)
            print("play stream..")
            playPauseBtn.tag = 0
        } else {
            stopAudio()
            playPauseBtn.setBackgroundImage(UIImage(named:"play_icon.png"),for:UIControlState.normal)
            print("pause stream..")
            playPauseBtn.tag = 1
        }
    }
    
    @IBAction func contactBtnClicked(_ sender: Any) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ContactController") as! ContactController
        self.navigationController?.pushViewController(nextViewController, animated:true)
    }
    
    func setupRemoteTransportControls() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()
        
        // Add handler for Play Command
        commandCenter.playCommand.addTarget { [unowned self] event in
            if self.player.rate == 0.0 {
                self.player.play()
                return .success
            }
            return .commandFailed
        }
        
        // Add handler for Pause Command
        commandCenter.pauseCommand.addTarget { [unowned self] event in
            if self.player.rate == 1.0 {
                self.player.pause()
                return .success
            }
            return .commandFailed
        }
    }
    
    func setupNowPlaying(image: UIImage) {
        // Define Now Playing Info
        var nowPlayingInfo = [String : Any]()
        nowPlayingInfo[MPMediaItemPropertyTitle] = "Best Radio 926"
        
        nowPlayingInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(boundsSize: image.size) { size in
                return image
        }
        
        nowPlayingInfo[MPNowPlayingInfoPropertyPlaybackRate] = player.rate
        
        // Set the metadata
        MPNowPlayingInfoCenter.default().nowPlayingInfo = nowPlayingInfo
    }
}
