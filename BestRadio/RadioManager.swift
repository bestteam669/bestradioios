//
//  RadioManager.swift
//  BestRadio
//
//  Created by Admin on 07/10/2018.
//  Copyright © 2018 SA Cowabunga. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import SwiftyJSON

class RadioManager {
    
    static let instance = RadioManager()
    var isTimerRunning = false
    
    func startTimer() {
        
        let currSecs = (60-DayTimeUtils.getCurrentMinute())*60
        let currDate = Date().addingTimeInterval(TimeInterval(currSecs))
        let timer = Timer.init(fireAt: currDate, interval: 3600.0, target: self, selector: #selector(fetchProducerInfo), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: .commonModes)
        isTimerRunning = true
    }
    
    @objc func fetchProducerInfo() {
        print("fetching data..")
        let fireBaseRef = Database.database().reference()
        let currDay = DayTimeUtils.getDayOfWeek()
        let isWeekend = currDay == "Saturday" || currDay == "Sunday"
        let index = DayTimeUtils.getFirebaseIndex(isWeekend: isWeekend, currentHour: DayTimeUtils.getCurrentHour())
        let path = "schedule/" + currDay + "/" + String(index)
        let _ = fireBaseRef.child(path).observe(.value, with: { (snapshot) in
            
            let json = JSON(snapshot.value as Any)
            let userinfo : [String:JSON] = ["JSON": json]
            NotificationCenter.default.post(name: NSNotification.Name.init("ProducerInfo"), object: nil, userInfo: userinfo)
        })
    }
}
