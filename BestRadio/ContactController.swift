//
//  ContactController.swift
//  BestRadio
//
//  Created by Admin on 30/09/2018.
//  Copyright © 2018 SA Cowabunga. All rights reserved.
//

import UIKit
import MoPub

class ContactController: UIViewController {

    
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var youTubeBtn: UIButton!
    @IBOutlet weak var webBtn: UIButton!
    @IBOutlet weak var insta: UIButton!
    @IBOutlet weak var videoAd: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawCircleBottom()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func twitterBtnClicked(_ sender: Any) {
        
        openURL(url: "https://twitter.com/BestRadio_926")
    }
    
    @IBAction func fbBtnClicked(_ sender: Any) {
        
        openURL(url: "https://www.facebook.com/bestradio926athens/")
    }
    
    @IBAction func youTubeBtnClicked(_ sender: Any) {
        
       openURL(url: "https://www.youtube.com/channel/UCu6vgL2rQeKTvmdES0dbPzw")
    }
    
    @IBAction func webBtnClicked(_ sender: Any) {
        
     openURL(url: "http://best926.gr/")
    }
    
    @IBAction func instaBtnClicked(_ sender: Any) {
        
       openURL(url: "https://www.instagram.com/bestradio926/")
    }
    
    @IBAction func adBtnClicked(_ sender: Any) {
        // show ad
        MPRewardedVideo.setDelegate(self, forAdUnitId: "3e15052dc5534b90acafa5a51bc118c8")
        MPRewardedVideo.loadAd(withAdUnitID: "3e15052dc5534b90acafa5a51bc118c8", withMediationSettings: nil)
    }
    
    func openURL(url: String) {
        
        guard let validatedUrl = URL(string: url) else {
            return //be safe
        }
        UIApplication.shared.open(validatedUrl, options: [:], completionHandler: nil)
    }
    
    func drawCircleBottom() {
        let screenWidth = self.view.frame.width
        let screenHeight = self.view.frame.height
        let hiddenSize = screenWidth / 4
        
        
        let circleView = UIView(frame: CGRect.init(x: 50 + hiddenSize, y: (screenHeight - screenWidth) + hiddenSize, width: screenWidth, height: screenWidth))
        circleView.backgroundColor = UIColor.init(hex: "ab1313") //ab1313 (171,19,19)
        circleView.layer.cornerRadius = circleView.frame.width/2
        circleView.clipsToBounds = true
        
        self.view.addSubview(circleView)
    }
}

extension ContactController: MPRewardedVideoDelegate {
    func rewardedVideoAdDidLoad(forAdUnitID adUnitID: String!) {
        if MPRewardedVideo.hasAdAvailable(forAdUnitID: adUnitID) {
            MPRewardedVideo.presentAd(forAdUnitID: adUnitID, from: self, with: nil)
        }
    }
}
