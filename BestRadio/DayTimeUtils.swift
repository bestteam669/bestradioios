//
//  DateTimeUtils.swift
//  BestRadio
//
//  Created by Admin on 25/09/2018.
//  Copyright © 2018 SA Cowabunga. All rights reserved.
//

import Foundation

class DayTimeUtils {
    
    static let mRegularTimeLines: [Int] = [0, 1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 21, 23, 24]
    static let mWeekendTimeLines: [Int] = [0, 2, 4, 5, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24]
    
    
    static func getDayOfWeek() -> String {
        var currDay : String
        let weekday = Calendar.current.component(.weekday, from: Date())
        print(weekday)
        switch weekday {
        case 1:
            currDay = "Sunday"
        case 2:
            currDay = "Monday"
        case 3:
            currDay = "Tuesday"
        case 4:
            currDay = "Wednesday"
        case 5:
            currDay = "Thursday"
        case 6:
            currDay = "Friday"
        default:
            currDay = "Saturday"
        }
        print(currDay)
        return currDay
    }
    
    static func getCurrentHour() -> Int {
        let date = Date()
        let calendar = Calendar.current
        return calendar.component(.hour, from: date)
    }
    
    static func getCurrentMinute() -> Int {
        let date = Date()
        let calendar = Calendar.current
        return calendar.component(.minute, from: date)
    }
    
    static func getCurrTime2() {
        //        print(UTCToLocal(date: "13:58:00"))
        print("--------------------------------")
        let dateString = "2017-10-10 15:56:25"
        let date = self.localToUTC(date: dateString)
        print("utc date is: \(date)")
    }
    
    static func UTCToLocal(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "h:mm a"
        
        return dateFormatter.string(from: dt!)
    }
    
    static func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "yyyy-MMM-dd hh:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }
    
    static func generateRegularDaySchedule() -> [Int:Int] {
        var regularPair = [Int:Int]()
        regularPair[mRegularTimeLines[0]] = mRegularTimeLines[1]
        regularPair[mRegularTimeLines[1]] = mRegularTimeLines[2]
        regularPair[mRegularTimeLines[2]] = mRegularTimeLines[3]
        regularPair[mRegularTimeLines[3]] = mRegularTimeLines[4]
        regularPair[mRegularTimeLines[4]] = mRegularTimeLines[5]
        regularPair[mRegularTimeLines[5]] = mRegularTimeLines[6]
        regularPair[mRegularTimeLines[6]] = mRegularTimeLines[7]
        regularPair[mRegularTimeLines[7]] = mRegularTimeLines[8]
        regularPair[mRegularTimeLines[8]] = mRegularTimeLines[9]
        regularPair[mRegularTimeLines[9]] = mRegularTimeLines[10]
        regularPair[mRegularTimeLines[10]] = mRegularTimeLines[11]
        regularPair[mRegularTimeLines[11]] = mRegularTimeLines[12]
        regularPair[mRegularTimeLines[12]] = mRegularTimeLines[13]
        regularPair[mRegularTimeLines[13]] = mRegularTimeLines[14]
        return regularPair
    }
    
    static func generateWeekendSchedule() -> [Int:Int] {
        var weekendMap = [Int:Int]()
        weekendMap[mWeekendTimeLines[0]] = mWeekendTimeLines[1]
        weekendMap[mWeekendTimeLines[1]] = mWeekendTimeLines[2]
        weekendMap[mWeekendTimeLines[2]] = mWeekendTimeLines[3]
        weekendMap[mWeekendTimeLines[3]] = mWeekendTimeLines[4]
        weekendMap[mWeekendTimeLines[4]] = mWeekendTimeLines[5]
        weekendMap[mWeekendTimeLines[5]] = mWeekendTimeLines[6]
        weekendMap[mWeekendTimeLines[6]] = mWeekendTimeLines[7]
        weekendMap[mWeekendTimeLines[7]] = mWeekendTimeLines[8]
        weekendMap[mWeekendTimeLines[8]] = mWeekendTimeLines[9]
        weekendMap[mWeekendTimeLines[9]] = mWeekendTimeLines[10]
        weekendMap[mWeekendTimeLines[10]] = mWeekendTimeLines[11]
        weekendMap[mWeekendTimeLines[11]] = mWeekendTimeLines[12]
        weekendMap[mWeekendTimeLines[12]] = mWeekendTimeLines[13]
        return weekendMap;
    }
    
    static func getFirebaseIndex(isWeekend: Bool, currentHour: Int) -> Int {
        
        var index = -1;
        
        if (isWeekend) {
            for line in 0...mWeekendTimeLines.count - 1 {
                
                let startTimeLine = mWeekendTimeLines[line];
                let endTimeLine = mWeekendTimeLines[line + 1];
                
                if (currentHour >= startTimeLine && currentHour < endTimeLine) {
                    index = line;
                    break;
                }
            }
        } else {
            for line in 0...mRegularTimeLines.count - 1 {
                let startTimeLine = mRegularTimeLines[line];
                let endTimeLine = mRegularTimeLines[line + 1];
                
                if (currentHour >= startTimeLine && currentHour < endTimeLine) {
                    index = line;
                    break;
                }
            }
        }
        return index;
    }
    
}
